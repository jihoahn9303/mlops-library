from setuptools import setup, find_namespace_packages

setup(
    name="mlopslib",
    version="0.0.1",
    description="Custom library for GCP Cloud Storage",
    url="https://gitlab.com/jihoahn9303/mlops-library",
    author="Jiho Ahn",
    packages=find_namespace_packages(where='gcp_cloud_storage_client_lib'),
    install_requires=[
        "google-cloud-storage==2.14.0"
    ]
)